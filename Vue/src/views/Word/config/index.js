export const menuIconList = [
  { icon: "icon-baocun", title: "保存" },
  { icon: "icon-dayin", title: "打印" },
  { icon: "icon-lishi", title: "历史记录" },
  { icon: "icon-chexiao", title: "撤销" },
  { icon: "icon-zhongzuo", title: "重做" },
];

export const menuTextList = [
  "开始",
  "插入",
  "页面",
  "引用",
  "审阅",
  "视图",
  "工具",
  "会员专享",
];

export const active1 = [
  { icon: "icon-geshishua", title: "格式刷" },
  { icon: "icon-fuzhi", title: "复制" },
  { icon: "icon-niantie", title: "粘贴" },
  { icon: "icon-jianqie", title: "剪切" },
  { icon: "icon-cachu", title: "清除" },
  { icon: "icon-anjianfengexian", title: "" },
  // 下一个icon
  { icon: "icon-zihaojia", title: "增加字号" },
  { icon: "icon-zihaojian", title: "减少字号" },
  { icon: "icon-cuti", title: "加粗" },
  { icon: "icon-italic", title: "斜体" },
  { icon: "icon-zitixiahuaxian", title: "下划线" },
  { icon: "icon-strikethrough", title: "删除线" },
  { icon: "icon-icon_tuchuxianshi", title: "高亮" },
  { icon: "icon-zitiyanse", title: "字体颜色" },
  { icon: "icon-anjianfengexian", title: "" },
  { icon: "icon-juzuoduiqi", title: "左对齐" },
  { icon: "icon-juzhongduiqi", title: "居中对齐" },
  { icon: "icon-juyouduiqi", title: "右对齐" },
  { icon: "icon-liangduanduiqi1", title: "两端对齐" },
  { icon: "icon-youxuliebiao", title: "有序列表" },
  { icon: "icon-wuxuliebiao", title: "无序列表" },
  { icon: "icon-anjianfengexian", title: "" },
];

export const fontFamily = [
  {
    label: "常用字体",
    list: [
      {
        value: "宋体",
        label: "宋体",
      },
      {
        value: "黑体",
        label: "黑体",
      },
    ],
  },
  {
    label: "所有字体",
    list: [
      {
        value: "微软雅黑",
        label: "微软雅黑",
      },
      {
        value: "宋体",
        label: "宋体",
      },
      {
        value: "黑体",
        label: "黑体",
      },
      {
        value: "仿宋",
        label: "仿宋",
      },
      {
        value: "楷体",
        label: "楷体",
      },
      {
        value: "等线",
        label: "等线",
      },
      { label: "华文琥珀", value: "华文琥珀" },
      { label: "华文楷体", value: "华文楷体" },
      { label: "华文隶书", value: "华文隶书" },
      { label: "华文新魏", value: "华文新魏" },
      { label: "华文行楷", value: "华文行楷" },
      { label: "华文中宋", value: "华文中宋" },
      { label: "华文彩云", value: "华文彩云" },
    ],
  },
];

export const fontSize = [
  { value: 56, label: "初号" },
  { value: 48, label: "小初" },
  { value: 34, label: "一号" },
  { value: 32, label: "小一" },
  { value: 29, label: "二号" },
  { value: 24, label: "小二" },
  { value: 21, label: "三号" },
  { value: 20, label: "小三" },
  { value: 18, label: "四号" },
  { value: 16, label: "小四" },
  { value: 14, label: "五号" },
  { value: 12, label: "小五" },
  { value: 10, label: "六号" },
  { value: 8, label: "小六" },
  { value: 7, label: "七号" },
  { value: 6, label: "八号" },
];

export const titleLevel = [
  { label: "正文", level: "none", value: "16px" },
  { label: "标题1", level: "first", value: "26px" },
  { label: "标题2", level: "second", value: "24px" },
  { label: "标题3", level: "third", value: "22px" },
  { label: "标题4", level: "fourth", value: "20px" },
  { label: "标题5", level: "fifth", value: "18px" },
  { label: "标题6", level: "sixth", value: "16px" },
];
