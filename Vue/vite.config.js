// vite.config.js
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { http_server_url } from "./default.config";
const path = require("path");
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    host: "0.0.0.0",
    port: 3000,
    // 生产不需要开启代理
    proxy: {
      "/baseURL": {
        target: http_server_url,
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/baseURL/, ""),
      },
    },
  },
  resolve: {
    alias: [
      {
        find: "@",
        replacement: path.resolve(__dirname, "src"),
      },
      {
        find: "@views",
        replacement: path.resolve(__dirname, "src/views"),
      },
      {
        find: "@compo",
        replacement: path.resolve(__dirname, "src/components"),
      },
      {
        find: "@el",
        replacement: path.resolve(__dirname, "src/el-components"),
      },
    ],
  },
  // 修改打包输出路径
  build: {
    outDir: "../Node/public/dist",
  },
});
